---
title: "Contact"
date: 2020-04-27 00:27:28 +0000 UTC
description: "Establish Contact with the Friends"
keywords:
draft: false
showLastmod: false
meta: false
vh50: false
type: "page"
---

Demonstrating the contact grid...

<div class="contact-grid">

<div class="contact-role">Benevolent Dictator</div>
<div class="contact-row-kind opaque" id="winny">Nickname</div>  <div class="contact-row-value">winny</div>
<div class="contact-row-kind opaque">Email</div>                <div class="contact-row-value"><a href="mailto:hello--@--winny.tech?subject=">hello--@--winny.tech</a></div>
<div class="contact-row-kind opaque">PGP</div>                  <div class="contact-row-value"><a href="">0000 0000 0000 0000 0000  0000 0000 0000 0000 0000</a></div>
<div class="contact-row-kind opaque">Languages</div>            <div class="contact-row-value">English</div>

<div class="contact-role">Workhorse</div>
<div class="contact-row-kind opaque" id="winny">Nickname</div>  <div class="contact-row-value">winny</div>
<div class="contact-row-kind opaque">Email</div>                <div class="contact-row-value"><a href="mailto:hello--@--winny.tech?subject=">hello--@--winny.tech</a></div>
<div class="contact-row-kind opaque">PGP</div>                  <div class="contact-row-value"><a href="">0000 0000 0000 0000 0000  0000 0000 0000 0000 0000</a></div>
<div class="contact-row-kind opaque">Languages</div>            <div class="contact-row-value">English</div>

<div class="contact-role">Pet</div>
<div class="contact-row-kind opaque" id="winny">Nickname</div>  <div class="contact-row-value">winny</div>
<div class="contact-row-kind opaque">Email</div>                <div class="contact-row-value"><a href="mailto:hello--@--winny.tech?subject=">hello--@--winny.tech</a></div>
<div class="contact-row-kind opaque">PGP</div>                  <div class="contact-row-value"><a href="">0000 0000 0000 0000 0000  0000 0000 0000 0000 0000</a></div>
<div class="contact-row-kind opaque">Languages</div>            <div class="contact-row-value">English</div>

</div>
