---
title: "Meta"
date: 2020-04-27 00:27:37 +0000 UTC
description: "Meta data..."
keywords:
draft: false
showLastmod: true
meta: false
vh50: false
type: "page"
---

### Website

This website is built with a static website engine called [Hugo](https://gohugo.io/) (version: [{{< hugo/version >}}](https://github.com/gohugoio/hugo)), using a theme called “[hehe](https://gitlab.com/friends.computer/hehe/)”, and a monospace typeface called [FiraCode](https://github.com/tonsky/FiraCode).

### Generic Grid

Demonstrating a generic grid

<div class="generic-grid gap-05">
<!--<div class="generic-grid-role">Core</div>-->
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A B C D E F</div><div class="generic-grid-row-value"></div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A B C D E</div>  <div class="generic-grid-row-value">F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A B C D</div>    <div class="generic-grid-row-value">E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A B C</div>      <div class="generic-grid-row-value">D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A B</div>        <div class="generic-grid-row-value">C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9 A</div>          <div class="generic-grid-row-value">B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8 9</div>            <div class="generic-grid-row-value">A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7 8</div>              <div class="generic-grid-row-value">9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6 7</div>                <div class="generic-grid-row-value">8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5 6</div>                  <div class="generic-grid-row-value">7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4 5</div>                    <div class="generic-grid-row-value">6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3 4</div>                      <div class="generic-grid-row-value">5 6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2 3</div>                        <div class="generic-grid-row-value">4 5 6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1 2</div>                          <div class="generic-grid-row-value">3 4 5 6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0 1</div>                            <div class="generic-grid-row-value">2 3 4 5 6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind">0</div>                              <div class="generic-grid-row-value">1 2 3 4 5 6 7 8 9 A B C D E F</div>
<div class="generic-grid-row-kind"></div>                               <div class="generic-grid-row-value">0 1 2 3 4 5 6 7 8 9 A B C D E F</div>
</div>

